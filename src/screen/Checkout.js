import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
  StyleSheet,
  StatusBar,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

const DATA = [
  {
    id: 'Bank Transfer',
    image: require('../assets/icons/tf.png'),
  },
  {
    id: 'OVO',
    image: require('../assets/icons/ovo.png'),
  },
  {
    id: 'Kartu Kredit',
    image: require('../assets/icons/kredit.png'),
  },
];

const Item = ({item, onPress, backgroundColor, textColor, selectedId}) => (
  <TouchableOpacity onPress={onPress} style={[styles.item, {backgroundColor}]}>
    <Image source={item.image} style={styles.image} />
    <Text style={[styles.id, {color: textColor}]}>{item.id}</Text>
    {selectedId === item.id ? (
      <AntDesign name="checkcircle" style={styles.checklistIcon} />
    ) : null}
  </TouchableOpacity>
);

const CheckoutScreen = ({navigation}) => {
  const [selectedId, setSelectedId] = useState();
  const renderItem = ({item}) => {
    const backgroundColor =
      item.id === selectedId ? 'rgba(3, 66, 98, 0.16)' : '#fff';
    const color = 'black';

    return (
      <Item
        item={item}
        onPress={() => setSelectedId(item.id)}
        backgroundColor={backgroundColor}
        textColor={color}
        selectedId={selectedId}
      />
    );
  };
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        <View
          style={{
            width: '100%',
            height: 153,
            backgroundColor: 'white',
            marginTop: 2,
            //marginLeft: 26,
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginTop: 18,
              marginLeft: 26,
            }}>
            Data Customer
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 14,
              fontWeight: '400',
              marginTop: 10,
              marginLeft: 26,
            }}>
            Agil Bani (0813763476)
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 14,
              fontWeight: '400',
              marginTop: 10,
              marginLeft: 26,
            }}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 14,
              fontWeight: '400',
              marginTop: 10,
              marginLeft: 26,
            }}>
            gantengdoang@dipanggang.com
          </Text>
        </View>

        <View
          style={{
            width: '100%',
            height: 107,
            backgroundColor: 'white',
            marginTop: 12,
            //marginLeft: 26,
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginTop: 18,
              marginLeft: 26,
            }}>
            Alamat Outlet Tujuan
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 14,
              fontWeight: '400',
              marginTop: 10,
              marginLeft: 26,
            }}>
            Jack Repair - Seturan (027-343457)
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 14,
              fontWeight: '400',
              marginTop: 10,
              marginLeft: 26,
            }}>
            Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
        </View>

        <View
          style={{
            width: '100%',
            height: 191,
            backgroundColor: '#fff',
            marginTop: 12,
            justifyContent: 'center',
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginLeft: 26,
            }}>
            Barang
          </Text>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginTop: 16, marginLeft: 24}}>
              <Image
                source={require('../assets/images/shoe.png')}
                style={{width: 84, height: 84}}
              />
            </View>
            <View
              style={{
                marginLeft: 15,
                width: '60%',
                justifyContent: 'center',
              }}>
              <Text style={{color: '#000', fontSize: 12, fontWeight: '500'}}>
                New Balance - Pink Abu - 40
              </Text>
              <Text
                style={{
                  marginTop: 11,
                  color: '#737373',
                  fontSize: 12,
                  fontWeight: '400',
                }}>
                Cuci Sepatu
              </Text>
              <Text
                style={{
                  color: '#737373',
                  fontSize: 12,
                  fontWeight: '400',
                  marginTop: 11,
                }}>
                Note : -
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 16,
              marginHorizontal: 26,
              justifyContent: 'space-between',
            }}>
            <Text style={{color: '#000', fontSize: 16, fontWeight: '400'}}>
              1 pasang
            </Text>
            <Text style={{color: '#000', fontSize: 16, fontWeight: '700'}}>
              @Rp 50.000
            </Text>
          </View>
        </View>

        <View
          style={{
            width: '100%',
            height: 124,
            backgroundColor: '#fff',
            marginTop: 12,
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginLeft: 26,
              marginTop: 8,
            }}>
            Rincian Pembayaran
          </Text>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 11,
              justifyContent: 'space-between',
              marginHorizontal: 26,
            }}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: '#000',
                  fontSize: 12,
                  fontWeight: '400',
                  //marginTop: 11,
                }}>
                Cuci Sepatu
              </Text>
              <Text
                style={{
                  color: '#FFC107',
                  fontWeight: '500',
                  fontSize: 12,
                  marginLeft: 15,
                }}>
                x1 Pasang
              </Text>
            </View>
            <Text>Rp 30.000</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 11,
              justifyContent: 'space-between',
              marginHorizontal: 26,
            }}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: '#000',
                  fontSize: 12,
                  fontWeight: '400',
                  //marginTop: 11,
                }}>
                Biaya Antar
              </Text>
            </View>
            <Text>Rp 3.000</Text>
          </View>
          <View
            style={{
              borderColor: '#EDEDED',
              borderBottomWidth: 1,
              marginTop: 10,
              marginHorizontal: 26,
            }}></View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 8,
              justifyContent: 'space-between',
              marginHorizontal: 26,
            }}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: '#000',
                  fontSize: 12,
                  fontWeight: '400',
                  //marginTop: 11,
                }}>
                Total
              </Text>
            </View>
            <Text style={{color: '#034262', fontWeight: '700'}}>Rp 33.000</Text>
          </View>
        </View>

        <View
          style={{
            width: '100%',
            height: 154,
            backgroundColor: '#fff',
            marginTop: 12,
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginLeft: 26,
              marginTop: 8,
            }}>
            Pilih Pembayaran
          </Text>
          <FlatList
            data={DATA}
            renderItem={renderItem}
            keyExtractor={item => item.id}
            extraData={selectedId}
            horizontal={true}
          />
        </View>
      </ScrollView>
      <TouchableOpacity
        style={{
          //width: '100%',
          marginTop: 35,
          backgroundColor: '#BB2427',
          borderRadius: 8,
          paddingVertical: 15,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 34,
          marginHorizontal: 20,
        }}
        onPress={() => navigation.navigate('Home')}>
        <Text
          style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          Pesan Sekarang
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    padding: 40,
    marginVertical: 8,
    marginHorizontal: 16,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#E1E1E1',
    borderRadius: 3,
  },
  image: {
    height: 31,
    width: 'ovo' ? 71 : 31,
    resizeMode: 'contain',
    alignItems: 'center',
    justifyContent: 'center',
  },
  checklistIcon: {
    fontSize: 24, // Adjust the icon size as needed
    color: '#034262', // Set the icon color
    //alignSelf: 'flex-end',
    position: 'absolute',
    right: 7,
    top: 5,
  },
});

export default CheckoutScreen;
