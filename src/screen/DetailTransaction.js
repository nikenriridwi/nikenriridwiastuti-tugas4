import React from 'react';
import {View, Text, Image, ScrollView} from 'react-native';

const DetailTransactionScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        <View
          style={{
            width: '100%',
            height: 238,
            backgroundColor: 'white',
            marginTop: 2,
          }}>
          <Text
            style={{
              color: '#BDBDBD',
              fontSize: 12,
              fontWeight: '500',
              textAlign: 'center',
              marginTop: 16,
            }}>
            20 Desember 2020 09.00
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 36,
              fontWeight: '700',
              letterSpacing: 0.257,
              marginTop: 56,
              textAlign: 'center',
            }}>
            CS122001
          </Text>
          <Text
            style={{
              color: '#000',
              fontSize: 14,
              fontWeight: '400',
              textAlign: 'center',
            }}>
            Kode Reservasi
          </Text>
          <Text
            style={{
              color: '#6F6F6F',
              fontSize: 14,
              fontWeight: '400',
              textAlign: 'center',
              marginTop: 42,
              letterSpacing: 0.257,
              marginHorizontal: 69,
            }}>
            Sebutkan Kode Reservasi saat tiba di outlet
          </Text>
        </View>

        <Text
          style={{
            color: '#201F26',
            fontSize: 14,
            fontWeight: '400',
            marginTop: 18,
            marginLeft: 11,
            letterSpacing: 0.257,
          }}>
          Barang
        </Text>
        <View
          style={{
            height: 133,
            backgroundColor: '#fff',
            borderRadius: 8,
            marginHorizontal: 11,
            marginTop: 16,
            justifyContent: 'center',
          }}>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginVertical: 6, marginLeft: 6}}>
              <Image
                source={require('../assets/images/shoe.png')}
                style={{width: 84, height: 84}}
              />
            </View>
            <View
              style={{
                marginLeft: 15,
                width: '60%',
                justifyContent: 'center',
              }}>
              <Text style={{color: '#000', fontSize: 12, fontWeight: '500'}}>
                New Balance - Pink Abu - 40
              </Text>
              <Text
                style={{
                  marginTop: 11,
                  color: '#737373',
                  fontSize: 12,
                  fontWeight: '400',
                }}>
                Cuci Sepatu
              </Text>
              <Text
                style={{
                  color: '#737373',
                  fontSize: 12,
                  fontWeight: '400',
                  marginTop: 11,
                }}>
                Note : -
              </Text>
            </View>
          </View>
        </View>

        <Text
          style={{
            color: '#201F26',
            fontSize: 14,
            fontWeight: '400',
            marginTop: 36,
            marginLeft: 11,
            letterSpacing: 0.257,
          }}>
          Status Pesanan
        </Text>
        <View
          style={{
            height: 78,
            backgroundColor: '#fff',
            borderRadius: 8,
            marginHorizontal: 11,
            marginTop: 16,
            justifyContent: 'center',
          }}>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginVertical: 3, marginLeft: 18}}>
              <Image
                source={require('../assets/icons/redcircle.png')}
                style={{width: 14, height: 14}}
              />
            </View>
            <View
              style={{
                marginLeft: 15,
                width: '60%',
                justifyContent: 'center',
              }}>
              <Text style={{color: '#201F26', fontSize: 12, fontWeight: '400'}}>
                Telah Reservasi
              </Text>
              <Text
                style={{
                  marginTop: 2,
                  color: '#A5A5A5',
                  fontSize: 10,
                  fontWeight: '400',
                  letterSpacing: 0.257,
                }}>
                20 Desember 2020
              </Text>
            </View>
            <Text
              style={{
                color: '#A5A5A5',
                fontSize: 10,
                fontWeight: '400',
                //marginTop: 11,
                marginLeft: 50,
                letterSpacing: 0.257,
              }}>
              09.00
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default DetailTransactionScreen;
