import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';

const FormulirScreen = ({navigation, route}) => {
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [toggleCheckBox2, setToggleCheckBox2] = useState(false);
  const [toggleCheckBox3, setToggleCheckBox3] = useState(false);
  const [toggleCheckBox4, setToggleCheckBox4] = useState(false);
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView>
        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Merek
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 7,
              marginHorizontal: 20,
              marginTop: 11,
            }}
            placeholder="Masukkan Merk Barang"
          />
        </View>
        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Warna
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 7,
              marginHorizontal: 20,
              marginTop: 11,
            }}
            placeholder="Warna Barang, cth: Merah - Putih"
          />
        </View>
        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Ukuran
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 7,
              marginHorizontal: 20,
              marginTop: 11,
            }}
            placeholder="Cth: S, M/39,40"
          />
        </View>
        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Photo
          </Text>
          <TouchableOpacity>
            <View
              style={{
                width: 84,
                height: 84,
                borderRadius: 8,
                marginLeft: 20,
                marginTop: 21,
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth: 1,
                borderColor: '#BB2427',
              }}>
              <Image
                source={require('../assets/icons/Camera.png')}
                style={{width: 20, height: 18}}
              />
            </View>
          </TouchableOpacity>
        </View>

        <View style={{marginLeft: 20, marginTop: 55}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <CheckBox
              disabled={false}
              value={toggleCheckBox}
              onValueChange={newValue => setToggleCheckBox(newValue)}
            />
            <Text
              style={{
                fontSize: 14,
                color: '#201f26',
                fontWeight: '400',
                lineHeight: 17,
                letterSpacing: 0.257,
              }}>
              Ganti Sol Sepatu
            </Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <CheckBox
              disabled={false}
              value={toggleCheckBox2}
              onValueChange={newValue2 => setToggleCheckBox2(newValue2)}
            />
            <Text
              style={{
                fontSize: 14,
                color: '#201f26',
                fontWeight: '400',
                lineHeight: 17,
                letterSpacing: 0.257,
              }}>
              Jahit Sepatu
            </Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <CheckBox
              disabled={false}
              value={toggleCheckBox3}
              onValueChange={newValue => setToggleCheckBox3(newValue)}
            />
            <Text
              style={{
                fontSize: 14,
                color: '#201f26',
                fontWeight: '400',
                lineHeight: 17,
                letterSpacing: 0.257,
              }}>
              Repaint Sepatu
            </Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <CheckBox
              disabled={false}
              value={toggleCheckBox4}
              onValueChange={newValue => setToggleCheckBox4(newValue)}
            />
            <Text
              style={{
                fontSize: 14,
                color: '#201f26',
                fontWeight: '400',
                lineHeight: 17,
                letterSpacing: 0.257,
              }}>
              Cuci Sepatu
            </Text>
          </View>
        </View>

        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Catatan
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 7,
              marginHorizontal: 20,
              marginTop: 11,
              height: 92,
              textAlignVertical: 'top',
            }}
            placeholder="Cth: ingin ganti sol baru"
          />
        </View>
      </ScrollView>

      <TouchableOpacity
        style={{
          //width: '100%',
          marginTop: 35,
          backgroundColor: '#BB2427',
          borderRadius: 8,
          paddingVertical: 15,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 34,
          marginHorizontal: 20,
        }}
        onPress={() =>
          navigation.navigate('HomeNavigation', {
            screen: 'Keranjang',
          })
        }>
        <Text
          style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          Masukkan Keranjang
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default FormulirScreen;
