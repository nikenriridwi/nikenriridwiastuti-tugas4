import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

const TransactionScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('TransactionNavigation', {screen: ' '})
          }>
          <View
            style={{
              margin: 11,
              borderRadius: 8,
              backgroundColor: 'white',
              height: 128,
            }}>
            <View
              style={{
                flexDirection: 'row',
                marginVertical: 18,
                justifyContent: 'space-between',
              }}>
              <View>
                <Text
                  style={{
                    color: '#BDBDBD',
                    fontSize: 12,
                    fontWeight: '500',
                    marginLeft: 10,
                  }}>
                  20 Desember 2020 09.00
                </Text>
                <Text
                  style={{
                    color: '#201F26',
                    fontSize: 12,
                    fontWeight: '500',
                    marginLeft: 10,
                    marginTop: 13,
                  }}>
                  New Balance - Pink Abu - 40
                </Text>
                <Text
                  style={{
                    color: '#201F26',
                    fontSize: 12,
                    fontWeight: '400',
                    marginLeft: 10,
                  }}>
                  Cuci Sepatu
                </Text>
                <Text
                  style={{
                    color: '#201F26',
                    fontSize: 12,
                    fontWeight: '400',
                    marginLeft: 10,
                    marginTop: 13,
                  }}>
                  Kode Reservasi :{' '}
                  <Text
                    style={{color: '#201F26', fontSize: 12, fontWeight: '700'}}>
                    CS201201
                  </Text>
                </Text>
              </View>
              <Text
                style={{
                  alignSelf: 'flex-end',
                  backgroundColor: 'rgba(242, 156, 31, 0.16)',
                  width: 81,
                  height: 21,
                  borderRadius: 10,
                  textAlign: 'center',
                  color: '#FFC107',
                  fontSize: 12,
                  fontWeight: '400',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                  marginRight: 7,
                }}>
                Reserved
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default TransactionScreen;
