import React from 'react';
import {View, Text, Image, ScrollView, TouchableOpacity} from 'react-native';

const ChartScreen = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        <View
          style={{
            height: 133,
            backgroundColor: '#fff',
            borderRadius: 8,
            margin: 11,
            justifyContent: 'center',
          }}>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginVertical: 6, marginLeft: 6}}>
              <Image
                source={require('../assets/images/shoe.png')}
                style={{width: 84, height: 84}}
              />
            </View>
            <View
              style={{
                marginLeft: 15,
                width: '60%',
                justifyContent: 'center',
              }}>
              <Text style={{color: '#000', fontSize: 12, fontWeight: '500'}}>
                New Balance - Pink Abu - 40
              </Text>
              <Text
                style={{
                  marginTop: 4,
                  color: '#737373',
                  fontSize: 12,
                  fontWeight: '400',
                }}>
                Cuci Sepatu
              </Text>
              <Text style={{color: '#737373', fontSize: 12, fontWeight: '400'}}>
                Note : -
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginTop: 45,
          }}>
          <Image
            source={require('../assets/icons/Plus.png')}
            style={{width: 20, height: 20}}
          />
          <Text
            style={{
              color: '#BB2427',
              fontSize: 14,
              fontWeight: '700',
              marginLeft: 8,
            }}>
            Tambah Barang
          </Text>
        </View>
      </ScrollView>
      <TouchableOpacity
        style={{
          //width: '100%',
          marginTop: 35,
          backgroundColor: '#BB2427',
          borderRadius: 8,
          paddingVertical: 15,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 34,
          marginHorizontal: 20,
        }}
        onPress={() =>
          navigation.navigate('HomeNavigation', {
            screen: 'Summary',
          })
        }>
        <Text
          style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          Selanjutnya
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default ChartScreen;
