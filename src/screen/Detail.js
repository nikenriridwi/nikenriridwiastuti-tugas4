import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ImageBackground,
} from 'react-native';

const DetailScreen = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
          behavior="padding" //tampilan form atau text input
          enabled
          keyboardVerticalOffset={-500}>
          <ImageBackground
            source={require('../assets/images/ImageBg2.png')} //load atau panggil asset image dari local
            style={{
              width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
              height: 317,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginTop: 28,
                marginHorizontal: 24,
              }}>
              <TouchableOpacity
                onPress={() => navigation.navigate('HomeNavigation')}>
                <Image
                  source={require('../assets/icons/back.png')}
                  style={{width: 24, height: 24}}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('HomeNavigation', {screen: 'Keranjang'})
                }>
                <Image
                  source={require('../assets/icons/Bag.png')}
                  style={{tintColor: 'white', width: 24, height: 24}}
                />
              </TouchableOpacity>
            </View>
          </ImageBackground>

          <View
            style={{
              width: '100%',
              backgroundColor: '#fff',
              borderTopLeftRadius: 19,
              borderTopRightRadius: 19,
              //paddingHorizontal: 20,
              paddingTop: 24,
              marginTop: -20,
            }}>
            <View style={{width: 225, height: 34, marginLeft: 33}}>
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 18,
                  fontWeight: '700',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                }}>
                Jack Repair Seturan
              </Text>
              <Image
                source={require('../assets/icons/star.png')}
                style={{
                  resizeMode: 'contain',
                  width: 65,
                  height: 10,
                  marginTop: 2,
                  marginLeft: -4,
                }}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 33,
                marginTop: 10,
              }}>
              <Image
                source={require('../assets/icons/location.png')}
                style={{width: 25, height: 25, marginLeft: -4}}
              />
              <Text
                style={{
                  color: '#979797',
                  fontSize: 10,
                  fontWeight: '400',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                }}>
                Jalan Affandi (Gejayan), No.15, Sleman{'\n'}Yogyakarta, 55384
              </Text>
              <Text
                style={{
                  color: '#3471CD',
                  fontSize: 12,
                  fontWeight: '700',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                  marginLeft: 16,
                }}>
                Lihat Maps
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                marginTop: 15,
                marginLeft: 33,
              }}>
              <Text
                style={{
                  backgroundColor: 'rgba(17, 168, 78, 0.12)',
                  borderRadius: 10,
                  width: 58,
                  padding: 3,
                  textAlign: 'center',
                  color: '#11A84E',
                  fontSize: 12,
                  fontWeight: '700',
                }}>
                BUKA
              </Text>
              <Text
                style={{
                  color: '#343434',
                  fontSize: 12,
                  fontWeight: '700',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                  marginLeft: 15,
                  textAlign: 'center',
                }}>
                09.00 - 21.00
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 17,
              }}>
              <View style={{flex: 1, height: 1, backgroundColor: '#EEE'}} />
              <View style={{flex: 1, height: 1, backgroundColor: '#EEE'}} />
            </View>

            <View>
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 16,
                  fontWeight: '500',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                  marginTop: 23,
                  marginHorizontal: 33,
                }}>
                Deskripsi
              </Text>
              <Text
                style={{
                  color: '#595959',
                  fontSize: 14,
                  fontWeight: '400',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                  marginTop: 10,
                  marginHorizontal: 33,
                }}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
                gravida mattis arcu interdum lectus egestas scelerisque. Blandit
                porttitor diam viverra amet nulla sodales aliquet est. Donec
                enim turpis rhoncus quis integer. Ullamcorper morbi donec
                tristique condimentum ornare imperdiet facilisi pretium
                molestie.
              </Text>
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 16,
                  fontWeight: '500',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                  marginTop: 23,
                  marginHorizontal: 33,
                }}>
                Range Biaya
              </Text>
              <Text
                style={{
                  color: '#8D8D8D',
                  fontSize: 16,
                  fontWeight: '500',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                  marginTop: 6,
                  marginHorizontal: 33,
                }}>
                Rp 20.000 - 80.000
              </Text>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
      <TouchableOpacity
        style={{
          //width: '100%',
          marginTop: 35,
          backgroundColor: '#BB2427',
          borderRadius: 8,
          paddingVertical: 15,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 34,
          marginHorizontal: 20,
        }}
        onPress={() =>
          navigation.navigate('HomeNavigation', {
            screen: 'Formulir Pendaftaran',
          })
        }>
        <Text
          style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          Repair Disini
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default DetailScreen;
