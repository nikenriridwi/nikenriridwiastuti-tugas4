import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';

const EditScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#FFF'}}>
      <ScrollView>
        <View
          style={{
            alignItems: 'center',
          }}>
          <Image
            source={require('../assets/icons/profpic.png')}
            style={{width: 95, height: 95, marginTop: 52}}
          />
          <TouchableOpacity>
            <View style={{flexDirection: 'row', marginTop: 17}}>
              <Image
                source={require('../assets/icons/edit.png')}
                style={{width: 24, height: 24}}
              />
              <Text style={{color: '#3A4BE0', fontSize: 18, fontWeight: '500'}}>
                Edit Foto
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={{marginHorizontal: 19, marginTop: 55}}>
          <Text style={{color: '#BB2427', fontSize: 12, fontWeight: '600'}}>
            Nama
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              height: 45,
              borderRadius: 7,
              marginTop: 11,
            }}
            placeholder="Agil Bani"
            keyboardType="default"
          />
        </View>
        <View style={{marginHorizontal: 19, marginTop: 27}}>
          <Text style={{color: '#BB2427', fontSize: 12, fontWeight: '600'}}>
            Email
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              height: 45,
              borderRadius: 7,
              marginTop: 11,
            }}
            placeholder="gilagil@gmail.com"
            keyboardType="email-address"
          />
        </View>
        <View style={{marginHorizontal: 19, marginTop: 27}}>
          <Text style={{color: '#BB2427', fontSize: 12, fontWeight: '600'}}>
            No hp
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              height: 45,
              borderRadius: 7,
              marginTop: 11,
            }}
            placeholder="08124564879"
            keyboardType="numeric"
          />
        </View>
      </ScrollView>

      <TouchableOpacity
        style={{
          //width: '100%',
          marginTop: 35,
          backgroundColor: '#BB2427',
          borderRadius: 8,
          paddingVertical: 15,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 34,
          marginHorizontal: 20,
        }}
        onPress={() =>
          navigation.navigate('ProfileNavigation', {
            screen: 'Profile',
          })
        }>
        <Text
          style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          Simpan
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default EditScreen;
