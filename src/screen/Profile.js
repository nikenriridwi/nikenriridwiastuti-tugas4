import React from 'react';
import {View, Text, Image, ScrollView, TouchableOpacity} from 'react-native';

const ProfileScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        <View
          style={{
            backgroundColor: 'white',
            width: '100%',
            height: 292,
            alignItems: 'center',
          }}>
          <Image
            source={require('../assets/icons/profpic.png')}
            style={{width: 95, height: 95, marginTop: 52}}
          />
          <Text
            style={{
              color: '#050152',
              fontSize: 20,
              fontWeight: '700',
              marginTop: 5,
            }}>
            Agil Bani
          </Text>
          <Text style={{color: '#A8A8A8', fontSize: 10, fontWeight: '500'}}>
            gilagil@gmail.com
          </Text>

          <TouchableOpacity
            onPress={() =>
              navigation.navigate('ProfileNavigation', {screen: 'Edit Profile'})
            }>
            <View
              style={{
                width: 64,
                height: 27,
                backgroundColor: '#F6F8FF',
                borderRadius: 18,
                justifyContent: 'center',
                marginTop: 21,
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 10,
                  color: '#050152',
                  fontWeight: '600',
                }}>
                Edit
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View
          style={{
            backgroundColor: 'white',
            height: 293,
            marginTop: 12,
            marginHorizontal: 17,
          }}>
          <TouchableOpacity>
            <Text
              style={{
                color: '#000',
                fontSize: 16,
                fontWeight: '500',
                marginLeft: 79,
                marginTop: 18,
              }}>
              About
            </Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text
              style={{
                color: '#000',
                fontSize: 16,
                fontWeight: '500',
                marginLeft: 79,
                marginTop: 37,
              }}>
              Terms & Condition
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('ProfileNavigation', {screen: 'FAQ'})
            }>
            <Text
              style={{
                color: '#000',
                fontSize: 16,
                fontWeight: '500',
                marginLeft: 79,
                marginTop: 37,
              }}>
              FAQ
            </Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text
              style={{
                color: '#000',
                fontSize: 16,
                fontWeight: '500',
                marginLeft: 79,
                marginTop: 37,
              }}>
              History
            </Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text
              style={{
                color: '#000',
                fontSize: 16,
                fontWeight: '500',
                marginLeft: 79,
                marginTop: 37,
              }}>
              Setting
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            backgroundColor: 'white',
            height: 47,
            marginTop: 12,
            marginHorizontal: 17,
            marginBottom: 66,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
          }}>
          <Image
            source={require('../assets/icons/log_out.png')}
            style={{width: 24, height: 24}}
          />
          <TouchableOpacity>
            <Text style={{color: '#EA3D3D', fontSize: 16, fontWeight: '500'}}>
              Log Out
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default ProfileScreen;
