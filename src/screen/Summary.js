import React from 'react';
import {View, Text, Image, ScrollView, TouchableOpacity} from 'react-native';

const SummaryScreen = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        <View
          style={{
            width: '100%',
            height: 153,
            backgroundColor: 'white',
            marginTop: 2,
            //marginLeft: 26,
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginTop: 18,
              marginLeft: 26,
            }}>
            Data Customer
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 14,
              fontWeight: '400',
              marginTop: 10,
              marginLeft: 26,
            }}>
            Agil Bani (0813763476)
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 14,
              fontWeight: '400',
              marginTop: 10,
              marginLeft: 26,
            }}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 14,
              fontWeight: '400',
              marginTop: 10,
              marginLeft: 26,
            }}>
            gantengdoang@dipanggang.com
          </Text>
        </View>

        <View
          style={{
            width: '100%',
            height: 107,
            backgroundColor: 'white',
            marginTop: 12,
            //marginLeft: 26,
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginTop: 18,
              marginLeft: 26,
            }}>
            Alamat Outlet Tujuan
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 14,
              fontWeight: '400',
              marginTop: 10,
              marginLeft: 26,
            }}>
            Jack Repair - Seturan (027-343457)
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 14,
              fontWeight: '400',
              marginTop: 10,
              marginLeft: 26,
            }}>
            Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
        </View>

        <View
          style={{
            width: '100%',
            height: 172,
            backgroundColor: '#fff',
            marginTop: 12,
            justifyContent: 'center',
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginLeft: 26,
            }}>
            Barang
          </Text>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginTop: 16, marginLeft: 24}}>
              <Image
                source={require('../assets/images/shoe.png')}
                style={{width: 84, height: 84}}
              />
            </View>
            <View
              style={{
                marginLeft: 15,
                width: '60%',
                justifyContent: 'center',
              }}>
              <Text style={{color: '#000', fontSize: 12, fontWeight: '500'}}>
                New Balance - Pink Abu - 40
              </Text>
              <Text
                style={{
                  marginTop: 11,
                  color: '#737373',
                  fontSize: 12,
                  fontWeight: '400',
                }}>
                Cuci Sepatu
              </Text>
              <Text
                style={{
                  color: '#737373',
                  fontSize: 12,
                  fontWeight: '400',
                  marginTop: 11,
                }}>
                Note : -
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
      <TouchableOpacity
        style={{
          //width: '100%',
          marginTop: 35,
          backgroundColor: '#BB2427',
          borderRadius: 8,
          paddingVertical: 15,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 34,
          marginHorizontal: 20,
        }}
        onPress={() =>
          navigation.navigate('HomeNavigation', {
            screen: 'Berhasil',
          })
        }>
        <Text
          style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          Reservasi Sekarang
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default SummaryScreen;
