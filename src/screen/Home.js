import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ImageBackground,
} from 'react-native';
import HomeNavigation from '../HomeNavigation';

const HomeScreen = ({navigation, route}) => {
  const [loved, setLoved] = useState(false);
  const [loved2, setLoved2] = useState(false);

  const handleLove = () => {
    setLoved(!loved);
  };

  const handleLove2 = () => {
    setLoved2(!loved2);
  };
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        <View
          style={{
            backgroundColor: '#fff',
            height: 296,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginTop: 56,
              marginLeft: 22,
              marginRight: 27,
            }}>
            <TouchableOpacity
              onPress={() => navigation.navigate('ProfileNavigation')}>
              <Image
                source={require('../assets/icons/profpic.png')}
                style={{width: 45, height: 45}}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('HomeNavigation', {screen: 'Keranjang'})
              }>
              <Image
                source={require('../assets/icons/Bag.png')}
                style={{
                  width: 18,
                  height: 20,
                }}
              />
            </TouchableOpacity>
          </View>
          <Text
            style={{
              color: '#034262',
              fontSize: 15,
              fontStyle: 'normal',
              fontWeight: '500',
              lineHeight: 36,
              marginLeft: 22,
            }}>
            Hello, Agil!
          </Text>
          <Text
            style={{
              color: '#0A0827',
              fontSize: 20,
              fontStyle: 'normal',
              fontWeight: '700',
              lineHeight: 32,
              marginLeft: 22,
            }}>
            Ingin merawat dan perbaiki sepatumu? Cari disini
          </Text>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 20,
              marginLeft: 20,
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: '#F6F8FF',
                //marginTop: 20,
                width: '78%',
                borderRadius: 10,
              }}>
              <Image
                source={require('../assets/icons/Search.png')}
                style={{
                  width: 16,
                  height: 17,
                  marginLeft: 10,
                  resizeMode: 'contain',
                }}
              />
              <TextInput
                style={{
                  height: 45,
                  width: 250,
                }}></TextInput>
            </View>

            <TouchableOpacity>
              <View
                style={{
                  width: 45,
                  height: 45,
                  backgroundColor: '#F6F8FF',
                  borderRadius: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginRight: 20,
                }}>
                <Image
                  source={require('../assets/icons/Filter.png')}
                  style={{
                    width: 16,
                    height: 15,
                    resizeMode: 'contain',
                  }}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>

        <View
          style={{
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
            marginTop: 17,
            marginHorizontal: 20,
          }}>
          <TouchableOpacity>
            <View
              style={{
                width: 95,
                height: 95,
                backgroundColor: '#fff',
                borderRadius: 16,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={require('../assets/icons/Sepatu.png')}
                style={{width: 45, height: 45}}
              />
              <Text
                style={{
                  color: '#BB2427',
                  textAlign: 'center',
                  fontSize: 9,
                  fontWeight: '600',
                  marginTop: 9,
                }}>
                Sepatu
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View
              style={{
                width: 95,
                height: 95,
                backgroundColor: '#fff',
                borderRadius: 16,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={require('../assets/icons/jacket.png')}
                style={{width: 45, height: 45}}
              />
              <Text
                style={{
                  color: '#BB2427',
                  textAlign: 'center',
                  fontSize: 9,
                  fontWeight: '600',
                  marginTop: 9,
                }}>
                Jacket
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View
              style={{
                width: 95,
                height: 95,
                backgroundColor: '#fff',
                borderRadius: 16,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={require('../assets/icons/Tas.png')}
                style={{width: 45, height: 45}}
              />
              <Text
                style={{
                  color: '#BB2427',
                  textAlign: 'center',
                  fontSize: 9,
                  fontWeight: '600',
                  marginTop: 9,
                }}>
                Tas
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View
          style={{
            justifyContent: 'space-between',
            marginHorizontal: 20,
            flexDirection: 'row',
            marginTop: 27,
          }}>
          <Text
            style={{
              color: '#0A0827',
              fontSize: 12,
              fontStyle: 'normal',
              fontWeight: '600',
            }}>
            Rekomendasi Terdekat
          </Text>
          <TouchableOpacity>
            <Text
              style={{
                color: '#E64C3C',
                fontSize: 10,
                fontStyle: 'normal',
                fontWeight: '500',
              }}>
              View all
            </Text>
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          onPress={() =>
            navigation.navigate('HomeNavigation', {screen: 'Detail'})
          }>
          <View
            style={{
              height: 133,
              backgroundColor: '#fff',
              borderRadius: 5,
              margin: 20,
            }}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <View style={{marginVertical: 6, marginLeft: 6}}>
                <Image
                  source={require('../assets/icons/shop.png')}
                  style={{width: 80, height: 121}}
                />
              </View>
              <View style={{marginLeft: 15, width: '60%', marginTop: 6}}>
                <Image
                  source={require('../assets/icons/star.png')}
                  style={{width: 50, height: 7, resizeMode: 'contain'}}
                />
                <Text
                  style={{color: '#D8D8D8', fontSize: 10, fontWeight: '500'}}>
                  4.8 ratings
                </Text>
                <Text
                  style={{
                    marginTop: 4,
                    color: '#201F26',
                    fontSize: 12,
                    fontWeight: '600',
                  }}>
                  Jack Repair Gejayan
                </Text>
                <Text
                  style={{color: '#D8D8D8', fontSize: 9, fontWeight: '500'}}>
                  Jl. Gejayan III No.2, Karangasem, Kec. Laweyan . . .
                </Text>
                <Text
                  style={{
                    backgroundColor: 'rgba(230, 76, 60, 0.20)',
                    borderRadius: 10,
                    width: 58,
                    padding: 3,
                    marginTop: 20,
                    textAlign: 'center',
                    color: '#EA3D3D',
                    fontSize: 12,
                    fontWeight: '700',
                  }}>
                  TUTUP
                </Text>
              </View>
              <View
                style={{alignSelf: 'flex-start', marginTop: 6, marginRight: 6}}>
                <TouchableOpacity onPress={handleLove}>
                  <Image
                    source={require('../assets/icons/Heart.png')}
                    style={{
                      width: 13,
                      height: 12,
                      resizeMode: 'contain',
                      tintColor: loved ? 'red' : '#ADADAD',
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('HomeNavigation', {screen: 'Detail'})
          }>
          <View
            style={{
              height: 133,
              backgroundColor: '#fff',
              borderRadius: 5,
              marginHorizontal: 20,
              marginBottom: 20,
            }}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <View style={{marginVertical: 6, marginLeft: 6}}>
                <Image
                  source={require('../assets/icons/shop2.png')}
                  style={{width: 80, height: 121}}
                />
              </View>

              <View style={{marginLeft: 15, width: '60%', marginTop: 6}}>
                <Image
                  source={require('../assets/icons/star.png')}
                  style={{width: 50, height: 7, resizeMode: 'contain'}}
                />
                <Text
                  style={{color: '#D8D8D8', fontSize: 10, fontWeight: '500'}}>
                  4.8 ratings
                </Text>
                <Text
                  style={{
                    marginTop: 4,
                    color: '#201F26',
                    fontSize: 12,
                    fontWeight: '600',
                  }}>
                  Jack Repair Seturan
                </Text>
                <Text
                  style={{color: '#D8D8D8', fontSize: 9, fontWeight: '500'}}>
                  Jl. Seturan, Kec. Laweyan . . .
                </Text>
                <Text
                  style={{
                    backgroundColor: 'rgba(17, 168, 78, 0.12)',
                    borderRadius: 10,
                    width: 58,
                    padding: 3,
                    marginTop: 20,
                    textAlign: 'center',
                    color: '#11A84E',
                    fontSize: 12,
                    fontWeight: '700',
                  }}>
                  BUKA
                </Text>
              </View>
              <View
                style={{alignSelf: 'flex-start', marginTop: 6, marginRight: 6}}>
                <TouchableOpacity onPress={handleLove2}>
                  <Image
                    source={require('../assets/icons/Heart.png')}
                    style={{
                      width: 13,
                      height: 12,
                      resizeMode: 'contain',
                      marginLeft: 6,
                      tintColor: loved2 ? 'red' : '#ADADAD',
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default HomeScreen;
