import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ProfileScreen from './screen/Profile';
import EditScreen from './screen/Edit';
import FaqScreen from './screen/Faq';

const Stack = createStackNavigator();

function ProfileNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Profile" component={ProfileScreen} />
      <Stack.Screen
        name="Edit Profile"
        component={EditScreen}
        options={{headerShown: true}}
      />
      <Stack.Screen
        name="FAQ"
        component={FaqScreen}
        options={{headerShown: true}}
      />
    </Stack.Navigator>
  );
}

export default ProfileNavigation;
