import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import TransactionScreen from './screen/Transaction';
import DetailTransactionScreen from './screen/DetailTransaction';

const Stack = createStackNavigator();

function TransactionNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="Transaksi"
        component={TransactionScreen}
        options={{headerShown: true}}
      />
      <Stack.Screen
        name=" "
        component={DetailTransactionScreen}
        options={{headerShown: true}}
      />
    </Stack.Navigator>
  );
}

export default TransactionNavigation;
