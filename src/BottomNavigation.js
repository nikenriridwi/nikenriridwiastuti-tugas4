import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeNavigation from './HomeNavigation';
import TransactionNavigation from './TransactionNavigation';
import ProfileNavigation from './ProfileNavigation';

const Tab = createBottomTabNavigator();

function TabMenu({isFocused, label}) {
  const icon = iconType(label);
  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 10,
      }}>
      <Image
        source={icon}
        style={{
          width: label === 'Transaction' ? 50 : 20,
          height: label === 'Transaction' ? 50 : 20,
          resizeMode: 'contain',
          tintColor: isFocused ? 'red' : 'grey',
        }}
      />
      <Text
        style={{
          color: isFocused ? 'red' : 'grey',
          marginTop: label === 'Transaction' ? -12 : 18,
        }}>
        {label}
      </Text>
    </View>
  );
}

function iconType(label) {
  switch (label) {
    case 'Home':
      return require('./assets/icons/home.png');
    case 'Transaction':
      return require('./assets/icons/transaction.png');
    case 'Profile':
      return require('./assets/icons/Profile.png');
  }
}

function MyTabBar({state, descriptors, navigation}) {
  return (
    <View style={{flexDirection: 'row'}}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            style={{
              flex: 1,
              backgroundColor: 'white',
              alignItems: 'center',
              justifyContent: 'center',
              height: 80,
            }}>
            <TabMenu isFocused={isFocused} label={label} />
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

export default function BottomNavigation() {
  return (
    <Tab.Navigator
      tabBar={props => <MyTabBar {...props} />}
      screenOptions={{headerShown: false}}>
      <Tab.Screen name="Home" component={HomeNavigation} />
      <Tab.Screen name="Transaction" component={TransactionNavigation} />
      <Tab.Screen name="Profile" component={ProfileNavigation} />
    </Tab.Navigator>
  );
}
