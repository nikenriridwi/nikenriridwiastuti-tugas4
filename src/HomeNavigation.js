import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './screen/Home';
import DetailScreen from './screen/Detail';
import FormulirScreen from './screen/Formulir';
import ChartScreen from './screen/Chart';
import SummaryScreen from './screen/Summary';
import SuccessScreen from './screen/Success';

const Stack = createStackNavigator();

function HomeNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="Detail" component={DetailScreen} />
      <Stack.Screen
        name="Formulir Pendaftaran"
        component={FormulirScreen}
        options={{headerShown: true}}
      />
      <Stack.Screen
        name="Keranjang"
        component={ChartScreen}
        options={{headerShown: true}}
      />
      <Stack.Screen
        name="Summary"
        component={SummaryScreen}
        options={{headerShown: true}}
      />

      <Stack.Screen name="Berhasil" component={SuccessScreen} />
    </Stack.Navigator>
  );
}

export default HomeNavigation;
